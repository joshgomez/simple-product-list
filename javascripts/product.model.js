
var Product = Backbone.Model.extend({

	defaults:
	{
		price 		: 0,
		amount 	 	: 0
	},
	
	validate: function( attributes )
	{
		if( attributes.name == 'undefined' || attributes.name == '')
		{
			return "Please fill in the product name.";
		}
	
		var price = parseInt(attributes.price);
		if( price < 0 || isNaN(price))
		{
			return "The product price can't be negative or none numeric.";
		}
		
		var amount = parseInt(attributes.amount);
		if(amount < 0 || isNaN(amount))
		{
			return "the amount of products in stock can't be negative.";
		}
	}
});