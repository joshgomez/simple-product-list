
var ProductView = Backbone.View.extend({

	events: {
	   "submit form" : 	"handleAddProduct",
	   "click button" : "handleDeleteProduct",
	   "click th" : 	"handleSortProduct"
	},
	 
	initialize: function(){
	
		this.templateFiles = [
			'templates/submit-add.html',
			'templates/product-body.html',
			'templates/product-row.html',
			'templates/product-row-empty.html'
		];
		
		this.templates = {};
		
		var self = this;
		
		$.each(this.templateFiles,function(index,tpl){
		
			$.get(tpl, function(html){
			
				self.templates[tpl] = html;
				self._updateLoadedTemplates();
			})
		
		});
		
		$(document).on("loadedTemplates", function(e){
		
			if(_.size(self.templates) == _.size(self.templateFiles))
			{
				self._preRender();
				self.render();
				
				self.collection.on('add', function(){
				
					this.renderMessage('Added product to the list!');
					this.render();
					
				},self);
				
				self.collection.on('remove sort', self.render,self);
				self.collection.on("invalid", self.renderError,self);
			}
		
		});
	},
	
	_updateLoadedTemplates:function(html)
	{
	
		$.event.trigger({
		
			type 			: "loadedTemplates",
			viewContext 	: this
		});
	
	},
	
	_preRender:function()
	{
	
		var template 	= 	_.template( this.templates['templates/submit-add.html'], {});
		template 		+= 	_.template( this.templates['templates/product-body.html'], {});
		this.$el.html( template );
	
	},
	
	renderError:function(models,error)
	{
		var $elSubmitMessage = this.$el.find('form+p');
		
		$elSubmitMessage.removeClass('success');
		$elSubmitMessage.addClass('failure');
		$elSubmitMessage.html(error);
	},
	
	renderMessage:function(msg)
	{
		var $elSubmitMessage = this.$el.find('form+p');
		
		$elSubmitMessage.removeClass('failure');
		$elSubmitMessage.addClass('success');
		$elSubmitMessage.html(msg);
	},
	
	render: function(){

		var htmlRows = ""
		
		var self = this;
		$.each(this.collection.models,function(index,model){
			
			var variables = { 
			
				name: _.escape(model.attributes.name),
				price: model.attributes.price,
				amount: model.attributes.amount,
				index: index
				
			};
			
			htmlRows += _.template( self.templates['templates/product-row.html'],variables);
		});
		
		if(!htmlRows)
			htmlRows = _.template( this.templates['templates/product-row-empty.html'],{});
		
		this.$el.find('table tbody').html( htmlRows );
		
	},
	
	//Events
	
	handleAddProduct: function(e){
	 
		e.preventDefault();
		var form = e.currentTarget;

		if(this.collection.addProduct(
			form.name.value,
			form.price.value,
			form.amount.value
		)) form.reset();
		
	},
	
	handleDeleteProduct: function(e){

		e.preventDefault();
		this.collection.deleteProduct(e.target.value)
	},
	
	handleSortProduct: function(e){

		e.preventDefault();
		
		var $t = $(e.target);
		var sort = $t.attr('data-sort');
		if(sort)
		{
			var $th = this.$el.find('table > thead > tr th');
			
			$th.removeClass("active");
			$th.removeClass("active");
			$t.addClass("active").toggleClass("up")

			this.collection.sortProduct(sort, $th.hasClass("active up"));
		}
	}
});


